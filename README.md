# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Get real time user count or visitor on a certain page
* Uses Pusher API

### How do I get set up? ###

1. For Laravel to work with Pusher, we need to get the pusher/pusher-php-server composer-package. So let’s install this first:
	- composer require pusher/pusher-php-server
2. Now open the file "config/broadcasting.php" and change the following lines within the options element. If you did not added the cluster or encryption in the previous section you can skip this step.
	- 	-'connections' => [

        'pusher' => [
            'driver' => 'pusher',
            'key' => env('PUSHER_APP_KEY'),
            'secret' => env('PUSHER_APP_SECRET'),
            'app_id' => env('PUSHER_APP_ID'),
            'options' => [
                'cluster' => 'eu',
                'encrypted' => true,
            ],
        ],
3. Now to activate these channels in this file you have to activate the BroadcastServiceProvider class in config/app.php file. You can find it in the heading of "Application Service Providers".
	- App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        App\Providers\BroadcastServiceProvider::class, // Uncomment this row.
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,

4. publish vendor
	- php artisan vendor:publish

5. update the \package\real-time-count-users\src\config\main.php "allowed_url" array to the desire url path so it will count the user with that current url path

6. Add this javascript code below the view of the url path route
	<script>
      //instantiate a Pusher object with our Credential's key
      var pusher = new Pusher('1ac550bb11f31558ae66', {
          cluster: 'ap1',
          encrypted: true
      });

      //Subscribe to the channel we specified in our Laravel Event
      var channel = pusher.subscribe('real-time-count-users');

      //Bind a function to a Event (the full Laravel class)
      channel.bind('Pusher\\RealTimeCountUser\\Events\\RealTimeCountUserEvent', countVisitor);

      function countVisitor(data) {
        $('#count').html(data.count);//this is the count selector in the html
      }
    </script>

7. add this "Pusher\RealTimeCountUser\RealTimeCountUserServiceProvider::class," to the config.app "providers"
8. add this , to the config.app 'aliases'
	'RTCUPusher'=> Pusher\RealTimeCountUser\RealTimeCountUserPusherFacade::class,
	'Pusher' => Pusher\Pusher::class,
