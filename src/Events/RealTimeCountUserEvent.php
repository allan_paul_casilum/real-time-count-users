<?php
namespace Pusher\RealTimeCountUser\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class RealTimeCountUserEvent implements ShouldBroadcast
{
    use InteractsWithSockets, SerializesModels;
	
	public $allowed_url_path;
	
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct()
    {
		$allowed_this_url = config('real-time-count-user.allowed_url');

		\RTCUPusher::setAllowedUrlToCount($allowed_this_url);
		\RTCUPusher::joiningVisitor();
    }
	
	public function broadcastWith()
    {
		$count_visitor = \RTCUPusher::countUser();
        // This must always be an array. Since it will be parsed with json_encode()
        return [
            'url_path' => \Request::path(),
			'count' => $count_visitor,
        ];
    }
	
    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
		return ['real-time-count-users'];
	}
}
