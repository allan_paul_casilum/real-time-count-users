<?php

namespace Pusher\RealTimeCountUser;

use Illuminate\Database\Eloquent\Model;

class RealTimeCountUserPusherModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'realtime_pusher_count';
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['session_id', 'user_agent', 'ip_address', 'url_path'];
}
