<?php

namespace Pusher\RealTimeCountUser\Middleware;

use Closure;

class RealTimeCountUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        event(new \Pusher\RealTimeCountUser\Events\RealTimeCountUserEvent());
		return $next($request);
    }
}
