<?php
namespace Pusher\RealTimeCountUser;
use App;

class RealTimeCountUserPusher 
{
	public $allowed_url_to_count;
	
	public function __construct()
	{
		$this->removeUsers();
	}
	
	public function setAllowedUrlToCount($array_allowed_url)
	{
		$this->allowed_url_to_count = $array_allowed_url;
	}
	
	public function getAllowedUrlToCount()
	{
		return $this->allowed_url_to_count;
	}
	
	public function getSessionId()
	{
		return session()->getId();
	}
	
	public function getIP()
	{
		return \Request::ip();
	}
	
	public function getDBIP()
	{
		$ip_address = $this->getIP();
		return \Pusher\RealTimeCountUser\RealTimeCountUserPusherModel::where('ip_address', $ip_address);
	}
	
	public function checkIpExists()
	{
		$ip_address = $this->getIP();
		return $this->getDBIP();
	}
	
	public function getUrlPath()
	{
		return \Request::path();
	}
	
	public function _isCurrentPageAllowed()
	{
		$allow_this_url = $this->getAllowedUrlToCount();
		$current_path = $this->getUrlPath();
		return in_array($current_path, $allow_this_url);
	}
	
	
	public function joiningVisitor()
	{
		$allow_this_url = $this->getAllowedUrlToCount();
		$current_path = $this->getUrlPath();
		if( $this->_isCurrentPageAllowed() ) {
			$joining = $this->_joiningVisitor($current_path, $allow_this_url);
		}
	}
	
	private function _joiningVisitor($current_path, $allow_this_url)
	{
		$session_id = $this->getSessionId();
		$check_ip = $this->queryIPUrlPath()->count();
		if( in_array($current_path, $allow_this_url) 
			&& $check_ip == 0
		) {
			\Pusher\RealTimeCountUser\RealTimeCountUserPusherModel::updateOrCreate(
				[
					'ip_address' => \Request::ip()
				],
				[
					'session_id' => $session_id,
					'user_agent' => \Request::header('User-Agent'),
					'ip_address' => \Request::ip(),
					'url_path' => $current_path,
				]
			);
			return true;
		}
		return false;
	}
	
	public function deleteSessionUrlPath()
	{
		$check_user = $this->getDBIP();
		if( $check_user->count() > 0 ){
			$ids = $check_user->pluck('id')->toArray();
			return \Pusher\RealTimeCountUser\RealTimeCountUserPusherModel::destroy($ids);
		}
		return false;
	}
	
	public function queryIPUrlPath()
	{
		$current_path = $this->getUrlPath();
		$ip_address = $this->getIP();
		return \Pusher\RealTimeCountUser\RealTimeCountUserPusherModel::where('ip_address', $ip_address)
				->where('url_path', $current_path);
	}
	
	public function querySessionUrlPath()
	{
		$current_path = $this->getUrlPath();
		$session_id = $this->getSessionId();
		return \Pusher\RealTimeCountUser\RealTimeCountUserPusherModel::where('session_id', $session_id)
				->where('url_path', $current_path);
	}
	
	public function countUser()
	{
		$current_path = $this->getUrlPath();
		$ip_address = $this->getDBIP();
		
		//perhaps the user is leaving
		if( !$this->_isCurrentPageAllowed() ) {
			//leaving
			$this->deleteSessionUrlPath();
		}
		
		return \Pusher\RealTimeCountUser\RealTimeCountUserPusherModel::where('url_path', $current_path)
				->distinct('url_path')
				->count();
	}
	
	public function removeUsers()
	{
		$date = new \DateTime;
		$date->modify('-60 minutes');
		$formatted = $date->format('Y-m-d H:i:s');
		\Pusher\RealTimeCountUser\RealTimeCountUserPusherModel::where('created_at', '<=', $formatted)->delete();
	}
}
