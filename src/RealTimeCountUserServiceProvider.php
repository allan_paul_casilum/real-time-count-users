<?php
namespace Pusher\RealTimeCountUser;

use Illuminate\Support\ServiceProvider;

class RealTimeCountUserServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
       $this->publishes([
			__DIR__ . '/migrations' => $this->app->databasePath() . '/migrations'
		], 'migrations');
		$this->publishes([
			__DIR__ . '/config' => config_path('real-time-count-user'),
		]);
	}

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
		$this->app->bind('RealTimeCountUserPusher', function() {
            return new RealTimeCountUserPusher;
        });
		$this->mergeConfigFrom(
			__DIR__ . '/config/main.php', 'real-time-count-user'
		);
    }
}
