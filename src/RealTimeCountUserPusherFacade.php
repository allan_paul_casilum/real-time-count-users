<?php 
namespace Pusher\RealTimeCountUser;
use Illuminate\Support\Facades\Facade;

class RealTimeCountUserPusherFacade extends Facade{
    protected static function getFacadeAccessor() { return 'RealTimeCountUserPusher'; }
}